<?php
    $indexpage = true;
    require "scripts/onimon/php/header.php";
?>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3 class="statistic" id="orport_connections">---</h3>
                <p>ORPort connections</p>
            </div>
            <div class="icon">
                <i class="ion ion-earth"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3 class="statistic" id="dirport_connections">---</h3>
                <p>DirPort connections</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-list"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3 class="statistic" id="traffic_down">---</h3>
                <p>Downstream traffic</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-cloud-download"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3 class="statistic" id="traffic_up">---</h3>
                <p>Upstream traffic</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-cloud-upload"></i>
            </div>
        </div>
    </div>
    <!-- ./col -->
</div>

<div class="row">
    <div class="col-md-12">
    <div class="box" id="traffic-over-time">
        <div class="box-header with-border">
          <h3 class="box-title">Traffic over time (in KiB/s)</h3>
        </div>
        <div class="box-body">
          <div class="chart">
            <canvas id="trafficOverTimeChart" width="800" height="250"></canvas>
          </div>
        </div>
        <div class="overlay">
          <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
    <div class="box" id="connections-over-time">
        <div class="box-header with-border">
          <h3 class="box-title">Connections over time</h3>
        </div>
        <div class="box-body">
          <div class="chart">
            <canvas id="connectionsOverTimeChart" width="800" height="250"></canvas>
          </div>
        </div>
        <div class="overlay">
          <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
    <div class="box" id="system-status">
        <!--div class="box-header with-border">
          <h3 class="box-title">System status</h3>
        </div-->
        <div class="box-body">
          <div class="form-group">
            <label>Runtime</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                <input type="text" class="form-control" disabled id="tor_runtime">
            </div>
          </div>
          <div class="form-group">
            <label>Address</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-wifi"></i></div>
                <input type="text" class="form-control" disabled id="tor_address">
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <div class="col-md-6">
    <div class="box" id="tor-status">
        <!--div class="box-header with-border">
          <h3 class="box-title">Tor status</h3>
        </div-->
        <div class="box-body">
          <div class="form-group">
            <label>Nickname</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                <input type="text" class="form-control" disabled id="tor_nickname">
            </div>
          </div>
          <div class="form-group">
            <label>Fingerprint</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-paw"></i></div>
                <input type="text" class="form-control" disabled id="tor_fingerprint">
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
    <div class="box" id="cpu-usage">
        <div class="box-header with-border">
          <h3 class="box-title">CPU Usage</h3>
        </div>
        <div class="box-body">
          <div class="chart">
            <canvas id="cpuUsageChart" width="400" height="150"></canvas>
          </div>
        </div>
        <div class="overlay">
          <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <div class="col-md-6">
    <div class="box" id="memory-usage">
        <div class="box-header with-border">
          <h3 class="box-title">Memory Usage</h3>
        </div>
        <div class="box-body">
          <div class="chart">
            <canvas id="memoryUsageChart" width="400" height="150"></canvas>
          </div>
        </div>
        <div class="overlay">
          <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>

<?php
if($boxedlayout)
{
    $tablelayout = "col-md-6";
}
else
{
    $tablelayout = "col-md-6 col-lg-4";
}
?>
<!--div class="row">
    <div class="<?php echo $tablelayout; ?>">
      <div class="box" id="FIXME">
        <div class="box-header with-border">
          <h3 class="box-title">FIXME</h3>
        </div>
        <!-- /.box-header -/->
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                    <th>FIXME</th>
                    <th>FIXME</th>
                    <th>FIXME</th>
                    </tr>
                  </tbody>
                </table>
            </div>
        </div>
        <div class="overlay">
          <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -/->
      </div>
      <!-- /.box -/->
    </div>
    <!-- /.col -/->
    <div class="<?php echo $tablelayout; ?>">
      <div class="box" id="FIXME">
        <div class="box-header with-border">
          <h3 class="box-title">FIXME</h3>
        </div>
        <!-- /.box-header -/->
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                    <th>FIXME</th>
                    <th>FIXME</th>
                    <th>FIXME</th>
                    </tr>
                  </tbody>
                </table>
            </div>
        </div>
        <div class="overlay">
          <i class="fa fa-refresh fa-spin"></i>
        </div>
        <!-- /.box-body -/->
      </div>
      <!-- /.box -/->
    </div>
    <!-- /.col -/->
</div-->
<!-- /.row -->
<?php
    require "scripts/onimon/php/footer.php";
?>

<script src="scripts/onimon/js/index.js"></script>
