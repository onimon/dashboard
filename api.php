<?php
    $api = true;
    require "scripts/onimon/php/password.php";
    require "scripts/onimon/php/auth.php";

    check_cors();

    include('scripts/onimon/php/data.php');
    header('Content-type: application/json');

    $data = array();

    // Non-Auth

    function bytes_format($value) {
        if ($value >= 1024**5)
            return number_format($value / 1024**5, 2) . " PiB";
        if ($value >= 1024**4)
            return number_format($value / 1024**4, 2) . " TiB";
        if ($value >= 1024**3)
            return number_format($value / 1024**3, 2) . " GiB";
        if ($value >= 1024**2)
            return number_format($value / 1024**2, 1) . " MiB";
        if ($value >= 1024**1)
            return number_format($value / 1024**1, 0) . " KiB";
        return $value . " B";
    }

    if (isset($_GET['summaryRaw'])) {
        $data = array_merge($data,  getSummaryData());
    }

    if (isset($_GET['summary']) || !count($_GET)) {
        $sum = getSummaryData();
        $sum['orport_connections'] = number_format($sum['orport_connections']);
        $sum['dirport_connections'] = number_format($sum['dirport_connections']);
        $sum['traffic_down'] = bytes_format($sum['traffic_down']);
        $sum['traffic_up'] = bytes_format($sum['traffic_up']);
        $data = array_merge($data,  $sum);
    }

    if (isset($_GET['status'])) {
        $data = array_merge($data,  getStatusData());
    }

    if (isset($_GET['overTimeData'])) {
        $data = array_merge($data,  getOverTimeData());
    }

    if (isset($_GET['overTimeData10mins'])) {
        $data = array_merge($data,  getOverTimeData10mins());
    }

    if (isset($_GET['cpuUsage'])) {
        $data = array_merge($data,  getProcessInfo());
    }

    if (isset($_GET['memoryUsage'])) {
        $data = array_merge($data,  getMemoryInfo());
    }

    if (isset($_GET['getAllNodes']) && $auth) {
        $data = array_merge($data, getAllNodes($_GET['getAllNodes']));
    }

    if (isset($_GET['tailLog']) && $auth) {
        $data = array_merge($data, tailTorLog($_GET['tailLog']));
    }

    function filterArray(&$inArray) {
        $outArray = array();
        foreach ($inArray as $key=>$value) {
            if (is_array($value)) {
                $outArray[htmlspecialchars($key)] = filterArray($value);
            } else {
                $outArray[htmlspecialchars($key)] = !is_numeric($value) ? htmlspecialchars($value) : $value;
            }
        }
        return $outArray;
    }

    $data = filterArray($data);

    if(isset($_GET["jsonForceObject"]))
    {
        echo json_encode($data, JSON_FORCE_OBJECT);
    }
    else
    {
        echo json_encode($data);
    }
?>
