<?php
    require "scripts/onimon/php/header.php";
?>

<!--
<div class="row">
    <div class="col-md-12">
        <button class="btn btn-info margin-bottom pull-right">Refresh Data</button>
    </div>
</div>
-->

<div class="row">
    <div class="col-md-12">
      <div class="box" id="or-network">
        <div class="box-header with-border">
          <h3 class="box-title">Known OR identities</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table id="all-nodes" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Nickname</th>
                            <th>Fingerprint</th>
                            <th>Published</th>
                            <th>Address</th>
                            <th>ORPort</th>
                            <th>DirPort</th>
                            <th>Flags</th>
                            <th>Bandwidth</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Nickname</th>
                            <th>Fingerprint</th>
                            <th>Published</th>
                            <th>Address</th>
                            <th>ORPort</th>
                            <th>DirPort</th>
                            <th>Flags</th>
                            <th>Bandwidth</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
       </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
</div>
<!-- /.row -->

<?php
    require "scripts/onimon/php/footer.php";
?>

<script src="scripts/onimon/js/network.js"></script>
