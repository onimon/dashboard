<?php
    require "scripts/onimon/php/header.php";

    if(strlen($pwhash) > 0)
    {
        $authenticationsystem = true;
    }
    else
    {
        $authenticationsystem = false;
    }
?>

<div class="row">
    <div class="col-md-12">
    <h1>Help center</h1>
    <h2>Header</h2>
    <h4>Top left: Status display</h4>
    <p>Shows different status messages:</p>
    <ul>
        <li>Status: Current status of Tor - Active (<i class="fa fa-circle" style="color:#7FFF00"></i>), Offline (<i class="fa fa-circle" style="color:#FF9900"></i>), or Stopped (<i class="fa fa-circle" style="color:#FF0000"></i>)</li>
        <li>Temp: Current CPU temperature</li>
        <li>Load: load averages for the last minute, 5 minutes and 15 minutes, respectively. A load average of 1 reflects the full workload of a single processor on the system. We show a red icon if the current load exceeds the number of available processors on this machine (which is <?php echo $nproc; ?>)</li>
        <li>Memory usage: Shows the percentage of memory actually blocked by applications. We show a red icon if the memory usage exceeds 75%</li>
    </ul>
    <h4>Top right: About</h4>
    <ul>
        <li>GitLab: Link to the OniMon repository</li>
        <li>Details: Link to the OniMon documentation</li>
        <li>Updates: Link to list of releases</li>
        <li>Update notifications: If updates are available, a link will be shown here.</li>
        <?php if($authenticationsystem){ ?>
        <li>Session timer: Shows the time remaining until the current login session expires.</li>
        <?php } ?>
    </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    <h2>Main page</h2>
    <p>On the main page, you can see various Tor statistics:</p>
    <ul>
        <li>Summary: A summary of statistics showing how many connections are currently established, and how much traffic was served in the running Tor instance. This summary is updated every 30 seconds.</li>
        <li>Traffic over time: Graph showing traffic speed (reads and writes) in KiB/s over 10 minute time intervals. More information can be acquired by hovering over the lines. This graph is updated every 10 minutes.</li>
        <li>Connections over time: Graph showing open connections (OR and Directory) over 10 minute time intervals. More information can be acquired by hovering over the lines. This graph is updated every 10 minutes.</li>
        <li>FIXME:
            <ul>
                <li>A: address lookup (most commonly used to map hostnames to an IPv4 address of the host)</li>
                <li>AAAA: address lookup (most commonly used to map hostnames to an IPv6 address of the host)</li>
                <li>PTR: most common use is for implementing reverse DNS lookups</li>
                <li>SRV: Service locator (often used by XMPP, SIP, and LDAP)</li>
                <li>and others</li>
            </ul>
        </li>
    </ul>
    <?php if($authenticationsystem){ ?>
    <p>Note that the login session does <em>not</em> expire on the main page, as the summary is updated every 10 seconds which refreshes the session.</p>
    <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    <h2>Onion Network</h2>
    <p>Shows the entire list of OR identities that are currently known by Tor. It is possible to search through the whole list by using the "Search" input field. The search filters against Nickname, Ident or Flags).</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    <h2>FIXME</h2>
    <p></p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    <h2>Tools &rarr; Tail logfile</h2>
    Live tailing of the raw Tor log.</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    <h2>Settings</h2>
    View and change Tor configuration
    <h4>FIXME</h4>

    <h4>FIXME</h4>

    </div>
</div>
<!--div class="row">
    <div class="col-md-12">
    <h2>Authentication system (currently <?php if($authenticationsystem) { ?>enabled<?php } else { ?>disabled<?php } ?>)</h2>
    <p></p>
    </div>
</div-->
<?php if($authenticationsystem) { ?>
<!--div class="row">
    <div class="col-md-12">
    <h2>Login / Logout</h2>
    <p>Using the Login / Logout function, a user can initiate / terminate a login session. The login page will also always be shown if a user tries to access a protected page directly without having a valid login session.</p>
    </div>
</div-->
<?php } ?>
<div class="row">
    <div class="col-md-12">
    <h2>Help (this page)</h2>
    Shows information about what is happening behind the scenes and what can be done with this web user interface (web UI). <!--The Help center will show details concerning the authentication system only if it is enabled.-->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    <h2>Footer</h2>
    Shows the currently installed Tor and OniMon Dashboard version. If an update is available, this will be indicated here.
    </div>
</div>

<?php
    require "scripts/onimon/php/footer.php";
?>
