<p align="center">
<a href="https://gitlab.com/onimon/dashboard"><img src="https://gitlab.com/onimon/dashboard/raw/master/img/logo.png" width="240"/></a>
</p>
<br/>


# OniMon - A simple Tor Relay Admin Dashboard.

## Overview

OniMon is a web interface to display status information of a running Tor Relay.
It is inspired from the
[Tor Arm](https://www.torproject.org/projects/arm.html.en) project and based on
the [Pi-Hole Admin Dashboard](https://github.com/pi-hole/AdminLTE).

The Dashboard executes the [onimon script](https://gitlab.com/onimon/script) on
the server running Tor to gather information about Tor status. The script opens
a control port connection for each request and is used to fetch Tor status,
traffic data and network details. It is written in Python and requires the
[Stem](https://stem.torproject.org/) module.

The Webserver must be able to run PHP scripts with
[APC](https://secure.php.net/manual/en/book.apc.php) enabled. APC is used
internally to cache Tor data in order to generate a traffic graph.

### Notes
* Data is gathered and filled into the cache as long as a Dashboard instance is
  accessing the webserver. This means that you must keep the dashboard open in
  your browser to have continuous graphs, and data will be missing for periods
  where the dashboard was not running.
* In general, you should limit access to the dashboard to the local network
  and only use it for monitoring tasks.

### Screenshots

<p align="center" ><a href="https://gitlab.com/onimon/dashboard/tree/master/img/screenshots">
<img src="https://gitlab.com/onimon/dashboard/raw/master/img/screenshots/screen-desktop.png" height="200" title="OniMon Dashboard - Desktop view"/>

<img src="https://gitlab.com/onimon/dashboard/raw/master/img/screenshots/screen-mobile.png" height="200" title="OniMon Dashboard - Mobile view"/>

<img src="https://gitlab.com/onimon/dashboard/raw/master/img/screenshots/screen-desktop-network.png" height="200" title="OniMon Dashboard - Onion Network"/>
</a></p>


## Installation

### Requirements

* [OniMon script](https://gitlab.com/onimon/script)
* Tor instance: tor
* Webserver: nginx, lighttpd or similar
* PHP5 with APC: php-fpm, php-apc

### Script

* Get the `onimon` script: [download](https://gitlab.com/onimon/script/raw/master/onimon) or check out the [repository](https://gitlab.com/onimon/script/).
* Follow the instructions in the [README](https://gitlab.com/onimon/script/blob/master/README.md).

### Dashboard

* Clone this repository into a new directory under your WWW root, e.g. `/var/www/html/admin`:

        git clone https://gitlab.com/onimon/dashboard.git /var/www/html/admin

* Set permissions to be accessible by the WWW user (```www-data``` on most systems):

        chown -R www-data /var/www/html/admin
        chmod -R ug+rw /var/www/html/admin

* Install a local web server with PHP5 and APC caching support. This example uses nginx on Debian:

        apt-get install nginx-full php-fpm php-apc
        systemctl start php-fpm
        systemctl start nginx

* Create a new site configuration for the OniMon Dashboard.
  - For nginx, use the [provided config](https://gitlab.com/onimon/dashboard/raw/master/nginx-example) and place it in `/etc/nginx/sites-enabled/onimon`.
  - Note that the `fastcgi_buffers` settings is required to allow nginx to handle the rather large HTTP queries in the Onion Network page.
  - Make sure to update any paths and the listening address/port as needed: nginx will listen for any connection on port `8080` by default - you likely want to limit access to your local network only.

* Create authentication credentials for accessing the Dashboard:

        htpasswd -c /etc/nginx/.htpasswd admin

* Reload your webserver to publish the new site.

        systemctl reload nginx

* Check that you can access the OniMon Dashboard at http://localhost:8080/admin/ (depending on your setup).
