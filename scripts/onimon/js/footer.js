// User menu toggle
$("#dropdown-menu a").on("click", function(event) {
    $(this).parent().toggleClass("open");
});
$("body").on("click", function(event) {
    if(!$("#dropdown-menu").is(event.target) && $("#dropdown-menu").has(event.target).length === 0) {
        $("#dropdown-menu").removeClass("open");
    }
});

//var torVersion = $("#torVersion").html();
var webVersion = $("#webVersion").html();

// Credit for following function: https://gist.github.com/alexey-bass/1115557
// Modified to discard any possible "v" in the string
function versionCompare(left, right) {
    if (typeof left + typeof right !== "stringstring")
    {
        return false;
    }

    // If we are on vDev then we assume that it is always
    // newer than the latest online release, i.e. version
    // comparison should return 1
    if(left === "vDev")
    {
        return 1;
    }

    var aa = left.split("v"),
        bb = right.split("v");

    var a = aa[aa.length-1].split(".")
        ,   b = bb[bb.length-1].split(".")
        ,   i = 0, len = Math.max(a.length, b.length);

    for (; i < len; i++) {
        if ((a[i] && !b[i] && parseInt(a[i]) > 0) || (parseInt(a[i]) > parseInt(b[i]))) {
            return 1;
        } else if ((b[i] && !a[i] && parseInt(b[i]) > 0) || (parseInt(a[i]) < parseInt(b[i]))) {
            return -1;
        }
    }
    return 0;
}


// Update check
$.getJSON("https://gitlab.com/api/v3/projects/2725725/repository/tags", function(json) {
    latestVersion = "v0.0";
    for (var i = 0; i < json.length; i++) {
        var thisVersion = json[i].name.slice(1);  // format 'vN.M'
        if (versionCompare(latestVersion, thisVersion) < 0)
            latestVersion = thisVersion;
    }
    if(versionCompare(webVersion, latestVersion) < 0) {
        // Alert user
        $("#webVersion").html($("#webVersion").text() + " <b><a class=\"alert-link\" href=\"https://gitlab.com/onimon/dashboard/tags/v"+latestVersion+"\">(Update available!)</a></b>");
        $("#alWebUpdate").show();
    }
});

// Session timer
var sessionvalidity = parseInt(document.getElementById("sessiontimercounter").textContent);
var start = new Date;

function updateSessionTimer()
{
    start = new Date;
    start.setSeconds(start.getSeconds() + sessionvalidity);
}

if(sessionvalidity > 0)
{
    // setSeconds will correctly handle wrap-around cases
    updateSessionTimer();

    setInterval(function() {
        var current = new Date;
        var totalseconds = (start - current) / 1000;

        // var hours = Math.floor(totalseconds / 3600);
        // totalseconds = totalseconds % 3600;

        var minutes = Math.floor(totalseconds / 60);
        if(minutes < 10){ minutes = "0" + minutes; }

        var seconds = Math.floor(totalseconds % 60);
        if(seconds < 10){ seconds = "0" + seconds; }

        if(totalseconds > 0)
        {
            document.getElementById("sessiontimercounter").textContent = minutes + ":" + seconds;
        }
        else
        {
            document.getElementById("sessiontimercounter").textContent = "-- : --";
        }

    }, 1000);
}
else
{
    document.getElementById("sessiontimer").style.display = "none";
}

// Handle Strg + Enter button on Login page
$(document).keypress(function(e) {
    if((e.keyCode === 10 || e.keyCode === 13) && e.ctrlKey && $("#loginpw").is(":focus")) {
        $("#loginform").attr("action", "settings.php");
        $("#loginform").submit();
    }
});

function testCookies()
{
    if (navigator.cookieEnabled)
    {
        return true;
    }

    // set and read cookie
    document.cookie = "cookietest=1";
    var ret = document.cookie.indexOf("cookietest=") !== -1;

    // delete cookie
    document.cookie = "cookietest=1; expires=Thu, 01-Jan-1970 00:00:01 GMT";

    return ret;
}

$(function() {
    if(!testCookies() && $("#cookieInfo").length)
    {
        $("#cookieInfo").show();
    }
});
