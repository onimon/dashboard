var tableApi;

function escapeRegex(text) {
  var map = {
    "(": "\\(",
    ")": "\\)",
    ".": "\\.",
  };
  return text.replace(/[().]/g, function(m) { return map[m]; });
}

function refreshData() {
    tableApi.ajax.url("api.php?getAllNodes").load();
//    updateSessionTimer();
}

function handleAjaxError( xhr, textStatus, error ) {
    if ( textStatus === "timeout" ) {
        alert( "The server took too long to send the data." );
    }
    else {
        alert( "An error occured while loading the data. Presumably your log is too large to be processed." );
    }
    $("#all-nodes_processing").hide();
    tableApi.clear();
    tableApi.draw();
}

// Credit for following function: http://stackoverflow.com/a/39460839
function base64ToBase16(base64) {
  return window.atob(base64)
      .split('')
      .map(function (char) {
        return ('0' + char.charCodeAt(0).toString(16)).slice(-2);
      })
     .join('')
     .toUpperCase();
}

$(document).ready(function() {
    var status;

    // Do we want to filter nodes?
    var GETDict = {};
    location.search.substr(1).split("&").forEach(function(item) {GETDict[item.split("=")[0]] = item.split("=")[1];});

    var APIstring = "api.php?getAllNodes";

    if("from" in GETDict)
    {
        APIstring += "&from="+GETDict["from"];
    }

    if("until" in GETDict)
    {
        APIstring += "&until="+GETDict["until"];
    }

    if("filter" in GETDict)
    {
        APIstring += "&filter="+GETDict["filter"];
    }

    tableApi = $("#all-nodes").DataTable( {
        "rowCallback": function( row, data, index ){
            // colorize by flags
            flags = data[6];
            if (flags.toLowerCase().indexOf("running") >= 0) {
                if (flags.toLowerCase().indexOf("exit") >= 0) {
                    $(row).css("color", "blue");
                }
                else {
                    $(row).css("color", "green");
                }
            }
            else {
                $(row).css("color", "red");
            }

            fp = data[1];
            $("td:eq(1)", row).html( "<a href=\"https://atlas.torproject.org/#details/"+fp+"\" title=\""+fp+"\">"+fp.substr(0,16)+"...</a>" );

            addr = data[3];
            $("td:eq(3)", row).html( "<a href=\"https://apps.db.ripe.net/search/query.html?searchtext="+addr+"&bflag=true&source=GRS#resultsAnchor\">"+addr+"</a>" );

            orport = data[4];
            dirport = data[5];
            if (orport <= 0) {
                $("td:eq(4)", row).html( "N/A" );
            }
            if (dirport <= 0) {
                $("td:eq(5)", row).html( "N/A" );
            }

            speed = data[7];
            speedConv = speed;
            speedUnit = "KiB/s";
            if (speed >= 1024) {
                speedConv /= 1024;
                speedUnit = "MiB/s";
            }
            $("td:eq(7)", row).html( "<span title=\""+speed+" KiB/s\">"+speedConv.toFixed(1)+"&nbsp;"+speedUnit+"</span>" );
            $("td:eq(7)", row).css("text-align", "right");
        },
        dom: "<'row'<'col-sm-12'f>>" +
             "<'row'<'col-sm-4'l><'col-sm-8'p>>" +
             "<'row'<'col-sm-12'tr>>" +
             "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        "ajax": {"url": APIstring, "error": handleAjaxError },
        "autoWidth" : false,
        "processing": true,
        "order" : [[2, "desc"]],
        "columns": [
            { "width" : "15%" },  // Name
            { "width" : "15%" },  // Ident
            { "width" : "15%", "type": "date" },  // Published
            { "width" : "10%" },  // Address
            { "width" :  "5%" },  // ORport
            { "width" :  "5%" },  // DirPort
            { "width" : "30%" },  // Flags
            { "width" :  "5%" },  // Bandwidth
        ],
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "columnDefs": [],
    });
    $("#all-nodes tbody").on( "click", "button", function () {
        var data = tableApi.row( $(this).parents("tr") ).data();
        // TODO
    } );

    if("name" in GETDict)
    {
        // Search in first column (zero indexed)
        tableApi.column(1).search("^"+escapeRegex(GETDict["name"])+"$",true,false);
    }
    if("ident" in GETDict)
    {
        // Search in second column (zero indexed)
        tableApi.column(2).search("^"+escapeRegex(GETDict["ident"])+"$",true,false);
    }
    if("flag" in GETDict)
    {
        // Search in sixth column (zero indexed)
        tableApi.column(6).search("^"+escapeRegex(GETDict["flag"])+"$",true,false);
    }
} );
