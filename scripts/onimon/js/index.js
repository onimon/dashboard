// Define global variables
var trafficTimeLineChart, connectionsTimeLineChart, cpuUsageChart, memoryUsageChart;

function padNumber(num) {
    return ("00" + num).substr(-2,2);
}

// Helper function needed for converting the Objects to Arrays

function objectToArray(p){
    var keys = Object.keys(p);
    keys.sort(function(a, b) {
        return a - b;
    });

    var arr = [], idx = [];
    for (var i = 0; i < keys.length; i++) {
        arr.push(p[keys[i]]);
        idx.push(keys[i]);
    }
    return [idx,arr];
}

// Functions to update data in page

function updateSummaryData(runOnce) {
    var setTimer = function(timeInSeconds) {
        if (!runOnce) {
            setTimeout(updateSummaryData, timeInSeconds * 1000);
        }
    };
    $.getJSON("api.php?summary", function LoadSummaryData(data) {

        ["orport_connections", "dirport_connections", "traffic_down", "traffic_up"].forEach(function(today) {
            var todayElement = $("h3#" + today);
            todayElement.text() !== data[today] && todayElement.addClass("glow");
        });

        window.setTimeout(function() {
            ["orport_connections", "dirport_connections", "traffic_down", "traffic_up"].forEach(function(header, idx) {
                var textData = data[header];
                $("h3#" + header).text(textData);
            });
            $("h3.statistic.glow").removeClass("glow");
        }, 500);

        updateSessionTimer();
    }).done(function() {
        setTimer(60);
    }).fail(function() {
        setTimer(300);
    });
}

function updateStatus() {
    $.getJSON("api.php?status", function(data) {

        ["tor_runtime", "tor_address", "tor_nickname", "tor_fingerprint"].forEach(function(status) {
            var statusElement = $("input#" + status);
            statusElement.val(data[status]);
        });

        updateSessionTimer();
    }).done(function() {
        setTimeout(updateStatus, 300000);
    }).fail(function() {
        setTimeout(updateStatus, 60000);
    });
}

var failures = 0;
function updateOverTime() {
    $.getJSON("api.php?overTimeData10mins", function(data) {
        // convert received objects to arrays
        data.traffic_down_over_time = objectToArray(data.traffic_down_over_time);
        data.traffic_up_over_time = objectToArray(data.traffic_up_over_time);
        // remove first data point since it is always zero
        data.traffic_down_over_time[0].splice(0,1);
        data.traffic_up_over_time[0].splice(0,1);
        // Remove possibly already existing data
        trafficTimeLineChart.data.labels = [];
        trafficTimeLineChart.data.datasets[0].data = [];
        trafficTimeLineChart.data.datasets[1].data = [];
        // Add data for each hour that is available
        for (var hour in data.traffic_down_over_time[0]) {
            if ({}.hasOwnProperty.call(data.traffic_down_over_time[0], hour)) {
                var h = parseInt(data.traffic_down_over_time[0][hour]);
                var d = new Date().setHours(Math.floor(h / 6), 10 * (h % 6), 0, 0);

                trafficTimeLineChart.data.labels.push(d);
                trafficTimeLineChart.data.datasets[0].data.push(data.traffic_down_over_time[1][hour]);
                trafficTimeLineChart.data.datasets[1].data.push(data.traffic_up_over_time[1][hour]);
            }
        }
        $("#traffic-over-time .overlay").remove();
        trafficTimeLineChart.update();

        // convert received objects to arrays
        data.connections_orport_over_time = objectToArray(data.connections_orport_over_time);
        data.connections_dirport_over_time = objectToArray(data.connections_dirport_over_time);
        // Remove possibly already existing data
        connectionsTimeLineChart.data.labels = [];
        connectionsTimeLineChart.data.datasets[0].data = [];
        connectionsTimeLineChart.data.datasets[1].data = [];
        // Add data for each hour that is available
        for (var hour in data.connections_orport_over_time[0]) {
            if ({}.hasOwnProperty.call(data.connections_orport_over_time[0], hour)) {
                var h = parseInt(data.connections_orport_over_time[0][hour]);
                var d = new Date();
                d.setHours(Math.floor((h % 144) / 6), 10 * (h % 6), 0, 0);
                d.setDate(Math.floor(h / 144));

                connectionsTimeLineChart.data.labels.push(d);
                connectionsTimeLineChart.data.datasets[0].data.push(data.connections_dirport_over_time[1][hour]);
                connectionsTimeLineChart.data.datasets[1].data.push(data.connections_orport_over_time[1][hour]);
            }
        }
        $("#connections-over-time .overlay").remove();
        connectionsTimeLineChart.update();
    }).done(function() {
        // Reload graph after 5 minutes
        failures = 0;
        setTimeout(updateOverTime, 300000);
    }).fail(function() {
        failures++;
        if(failures < 5)
        {
            // Try again after 1 minute only if this has not failed more
            // than five times in a row
            setTimeout(updateOverTime, 60000);
        }
    });
}

function updateCPU() {
    $.getJSON("api.php?cpuUsage", function(data) {
        var colors = [];
        // Get colors from AdminLTE
        $.each($.AdminLTE.options.colors, function(key, value) { colors.push(value); });
        // Remove possibly already existing data
        cpuUsageChart.data.datasets = [];
        // Collect and push values
        var v = [], c = [];
        colors.shift();
        v.push(data.tor_total_time); c.push(colors.shift());
        v.push(data.other_processes_time); c.push(colors.shift());
        v.push(data.total_idle_time); c.push(colors.shift());
        colors.shift();
        v.push(data.total_wait_time); c.push(colors.shift());
        // Build a single dataset with the data to be pushed
        var dd = {data: v, backgroundColor: c};
        // and push it at once
        cpuUsageChart.data.datasets.push(dd);
        $("#cpu-usage .overlay").remove();
        cpuUsageChart.update();
    }).done(function() {
        // Reload graph after 5 minutes
        failures = 0;
        setTimeout(updateCPU, 300000);
    }).fail(function() {
        setTimeout(updateCPU, 60000);
    });
}

function updateMemory() {
    $.getJSON("api.php?memoryUsage", function(data) {
        var colors = [];
        // Get colors from AdminLTE -> blue,red,green,lightblue,orange
        $.each($.AdminLTE.options.colors, function(key, value) { colors.push(value); });
        // Remove possibly already existing data
        memoryUsageChart.data.datasets = [];
        // Collect and push values
        var v = [], c = [];
        colors.shift();
        v.push(data.tor_effective_memory); c.push(colors.shift());
        v.push(data.other_processes_memory); c.push(colors.shift());
        v.push(data.free_memory); c.push(colors.shift());
        v.push(data.cached_memory); c.push(colors.shift());
        // Build a single dataset with the data to be pushed
        var dd = {data: v, backgroundColor: c};
        // and push it at once
        memoryUsageChart.data.datasets.push(dd);
        $("#memory-usage .overlay").remove();
        memoryUsageChart.update();
    }).done(function() {
        // Reload graph after 5 minutes
        setTimeout(updateMemory, 300000);
    }).fail(function() {
        setTimeout(updateMemory, 60000);
    });
}

// Credit: http://stackoverflow.com/questions/1787322/htmlspecialchars-equivalent-in-javascript/4835406#4835406
function escapeHtml(text) {
  var map = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    "\"": "&quot;",
    "\'": "&#039;"
  };

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

// Credit: https://stackoverflow.com/questions/1322732/convert-seconds-to-hh-mm-ss-with-javascript/31112615#31112615
Number.prototype.toHHMMSS = function() {
  var hours = Math.floor(this / 3600) < 10 ? ("00" + Math.floor(this / 3600)).slice(-2) : Math.floor(this / 3600);
  var minutes = ("00" + Math.floor((this % 3600) / 60)).slice(-2);
  var seconds = ("00" + (this % 3600) % 60).slice(-2);
  return hours + ":" + minutes + ":" + seconds;
}
Number.prototype.toHHMM = function() {
  var hours = Math.floor(this / 3600) < 10 ? ("00" + Math.floor(this / 3600)).slice(-2) : Math.floor(this / 3600);
  var minutes = ("00" + Math.floor((this % 3600) / 60)).slice(-2);
  return hours + ":" + minutes;
}

$(document).ready(function() {

        var isMobile = {
            Windows: function() {
                return /IEMobile/i.test(navigator.userAgent);
            },
            Android: function() {
                return /Android/i.test(navigator.userAgent);
            },
            BlackBerry: function() {
                return /BlackBerry/i.test(navigator.userAgent);
            },
            iOS: function() {
                return /iPhone|iPad|iPod/i.test(navigator.userAgent);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
            }
        };

        var ctx = document.getElementById("trafficOverTimeChart").getContext("2d");
        trafficTimeLineChart = new Chart(ctx, {
            type: "line",
            data: {
                labels: [],
                datasets: [
                    {
                        label: "Downstream",
                        fill: false,
                        //backgroundColor: "rgba(0,192,239,0.5)",
                        borderColor: "rgba(0, 166, 90,.8)",
                        pointBackgroundColor: "rgba(0, 166, 90,.8)",
                        pointBorderColor: "rgba(0,0,0,0.5)",
                        pointRadius: 1,
                        pointHoverRadius: 5,
                        data: [],
                        pointHitRadius: 5,
                        cubicInterpolationMode: "monotone"
                    },
                    {
                        label: "Upstream",
                        fill: true,
                        backgroundColor: "rgba(0,192,239,0.5)",
                        borderColor: "rgba(221, 75, 57,.8)",
                        pointBackgroundColor: "rgba(221, 75, 57,.8)",
                        pointBorderColor: "rgba(0,0,0,0.5)",
                        pointRadius: 1,
                        pointHoverRadius: 5,
                        data: [],
                        pointHitRadius: 5,
                        cubicInterpolationMode: "monotone"
                    },
                ]
            },
            options: {
                tooltips: {
                    enabled: true,
                    mode: "x-axis",
                    callbacks: {
                        title: function(tooltipItem, data) {
                            var label = tooltipItem[0].xLabel;
                            var time = label.match(/(\d?\d):?(\d?\d?)/);
                            var h = parseInt(time[1], 10);
                            var m = parseInt(time[2], 10) || 0;
                            var from = padNumber(h)+":"+padNumber(m)+":00";
                            var to = padNumber(h)+":"+padNumber(m+9)+":59";
                            return "Traffic from "+from+" to "+to;
                        },
                        label: function(tooltipItems, data) {
                            var percentage = 0.0;
                            var downstream = parseFloat(data.datasets[0].data[tooltipItems.index]);
                            var upstream = parseFloat(data.datasets[1].data[tooltipItems.index]);
                            var total = downstream + upstream;
                            var value = parseFloat(tooltipItems.yLabel);
                            if(total > 0)
                            {
                                percentage = 100.0*value/total;
                            }
                            return data.datasets[tooltipItems.datasetIndex].label + ": " + value.toFixed(1) + " KiB (" + percentage.toFixed(1) + "%)";
                        }
                    }
                },
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        type: "time",
                        time: {
                            unit: "hour",
                            displayFormats: {
                                hour: "HH:mm"
                            },
                            tooltipFormat: "HH:mm"
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                maintainAspectRatio: false
            }
        });

        var ctx = document.getElementById("connectionsOverTimeChart").getContext("2d");
        connectionsTimeLineChart = new Chart(ctx, {
            type: "line",
            data: {
                labels: [],
                datasets: [
                    {
                        label: "DirPort",
                        fill: false,
                        //backgroundColor: "rgba(220,220,220,0.5)",
                        borderColor: "rgba(234, 156, 18,.8)",
                        pointBackgroundColor: "rgba(234, 156, 18,.8)",
                        pointBorderColor: "rgba(0,0,0,0.5)",
                        pointRadius: 1,
                        pointHoverRadius: 5,
                        data: [],
                        pointHitRadius: 5,
                        cubicInterpolationMode: "monotone",
                    },
                    {
                        label: "ORPort",
                        fill: true,
                        backgroundColor: "rgba(220,220,220,0.5)",
                        borderColor: "rgba(0, 192, 239,.8)",
                        pointBackgroundColor: "rgba(0, 192, 239,.8)",
                        pointBorderColor: "rgba(0,0,0,0.5)",
                        pointRadius: 1,
                        pointHoverRadius: 5,
                        data: [],
                        pointHitRadius: 5,
                        cubicInterpolationMode: "monotone",
                    },

                ]
            },
            options: {
                tooltips: {
                    enabled: true,
                    mode: "x-axis",
                    callbacks: {
                        title: function(tooltipItem, data) {
                            var label = tooltipItem[0].xLabel;
                            var time = label.match(/(\d?\d):?(\d?\d?)/);
                            var h = parseInt(time[1], 10);
                            var m = parseInt(time[2], 10) || 0;
                            var from = padNumber(h)+":"+padNumber(m)+":00";
                            var to = padNumber(h)+":"+padNumber(m+9)+":59";
                            return "Connections from "+from+" to "+to;
                        },
                        label: function(tooltipItems, data) {
                            var value = parseInt(tooltipItems.yLabel);
                            return data.datasets[tooltipItems.datasetIndex].label + ": " + value;
                        }
                    }
                },
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        type: "time",
                        time: {
                            unit: "hour",
                            displayFormats: {
                                hour: "HH:mm"
                            },
                            tooltipFormat: "HH:mm"
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                maintainAspectRatio: false
            }
        });

        ctx = document.getElementById("cpuUsageChart").getContext("2d");
        cpuUsageChart = new Chart(ctx, {
            type: "doughnut",
            data: {
                labels: [
                    "Tor",
                    "Other",
                    "Idle",
                    "Waiting",
                ],
                datasets: [{ data: [] }]
            },
            options: {
                tooltips: {
                    mode: 'label',
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var idx = tooltipItem.index;
                            var value = parseFloat(data.datasets[0].data[idx]);
                            return data.labels[idx] + ": " + value.toHHMM() + ' hours';
                        }
                    }
                },
                legend: {
                    display: false
                },
                animation: {
                    duration: 2000
                },
                cutoutPercentage: 30
            }
        });

        ctx = document.getElementById("memoryUsageChart").getContext("2d");
        memoryUsageChart = new Chart(ctx, {
            type: "doughnut",
            data: {
                labels: [
                    "Tor",
                    "Other",
                    "Free",
                    "Cache",
                ],
                datasets: [{ data: [] }]
            },
            options: {
                tooltips: {
                    mode: 'label',
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var idx = tooltipItem.index;
                            var value = parseFloat(data.datasets[0].data[idx]);
                            return data.labels[idx] + ": " + (value / (1024*1024)).toFixed(1) + ' MiB';
                        }
                    }
                },
                legend: {
                    display: false
                },
                animation: {
                    duration: 2000
                },
                cutoutPercentage: 30
            }
        });

        // Pull in data via AJAX

        updateSummaryData();

        updateStatus();

        updateCPU();

        updateMemory();

        updateOverTime();
    });
