<?php
    $log = array();

    // Check if tor.log exists and is readable
    //$logListName = checkfile("/var/log/tor/log");
    //$log = new \SplFileObject($logListName);

    $TorPID = exec("onimon process");

    //error_log("Tor is running with PID " . $TorPID);

    $apc_cache_ttl = 10800;  // keep data for 3 hours
    $apc_cache_max_age = 21600;  // delete entries older than 6 hours

    if(isset($setupVars["API_PRIVACY_MODE"]))
    {
        $privacyMode = $setupVars["API_PRIVACY_MODE"];
    }
    else
    {
        $privacyMode = false;
    }

    // Credit: https://stackoverflow.com/questions/8273804/convert-seconds-into-days-hours-minutes-and-seconds/19680778#19680778
    function secondsToTime($seconds) {
        $sec = intval($seconds);
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$sec");
        return $dtF->diff($dtT)->format('%a days, %H:%i:%s hours');
    }

    /*******   Public Members ********/
    function getSummaryData() {

        // numer of open connections

        $orport_connections = getORConnections();
        $dirport_connections = getDirConnections();

        // traffic in kilobytes (KiB)

        $traffic_down = countTrafficDown();
        $traffic_up = countTrafficUp();

        return array(
            'orport_connections' => $orport_connections,
            'dirport_connections' => $dirport_connections,
            'traffic_down' => $traffic_down,
            'traffic_up' => $traffic_up,
        );
    }

    function getStatusData() {
        global $TorPID;

        $uptime = intval(exec("cat /proc/uptime | cut -d ' ' -f 1"));
        //$address = exec("curl http://ipecho.net/plain");

        $stat = parseProcStatForPID($TorPID);
        $runtime = $uptime - $stat['start_time'];

        $nickname = exec("onimon config Nickname");
        $address = exec("onimon address");
        $fingerprint = exec("onimon fingerprint");

        $fp = implode(' ', str_split($fingerprint, 4));

        return array(
            'tor_runtime' => secondsToTime($runtime),
            'tor_address' => $address,
            'tor_nickname' => $nickname,
            'tor_fingerprint' => $fp,
        );
    }

    function getConnectionsData() {
        global $apc_cache_ttl, $apc_cache_max_age;

        // retrieve cached values

        $success = false;
        $conn_orport_data = apc_fetch('onimon_connections_or', $success);
        if (! $success)
            $conn_orport_data = array();
        $conn_dirport_data = apc_fetch('onimon_connections_dir', $success);
        if (! $success)
            $conn_dirport_data = array();

        //error_log("cached onimon connections data has " . count($conn_orport_data) . " / " . count($conn_dirport_data) . " entries");

        $now = time();

        // clean up cache

        foreach($conn_orport_data as $key => $value) {
            if ($now - $key > $apc_cache_max_age) {
                unset($conn_orport_data[$key]);
            }
        }

        foreach($conn_dirport_data as $key => $value) {
            if ($now - $key > $apc_cache_max_age) {
                unset($conn_dirport_data[$key]);
            }
        }

        // only fetch new data if last cache entry is too old

        end($conn_orport_data); end($conn_dirport_data);  // point to last element
        $last_updated = $now - min(key($conn_orport_data), key($conn_dirport_data));  // also works for empty cache
        if ($last_updated >= 600) {
            $conn_orport_data[$now] = getORConnections();
            $conn_dirport_data[$now] = getDirConnections();

            //error_log("updating connections data since last entry is " . $last_updated . " s old");
        }

        // update internal cache

        apc_store('onimon_connections_or', $conn_orport_data, $apc_cache_ttl);
        apc_store('onimon_connections_dir', $conn_dirport_data, $apc_cache_ttl);

        return [$conn_orport_data,$conn_dirport_data];
    }

    function getTrafficData() {
        global $apc_cache_ttl, $apc_cache_max_age;

        // retrieve cached values

        $success = false;
        $traffic_down_data = apc_fetch('onimon_traffic_down', $success);
        if (! $success)
            $traffic_down_data = array();
        $traffic_up_data = apc_fetch('onimon_traffic_up', $success);
        if (! $success)
            $traffic_up_data = array();

        //error_log("cached onimon traffic data has " . count($traffic_down_data) . " / " . count($traffic_up_data) . " entries");

        $now = time();

        // clean up cache

        foreach($traffic_down_data as $key => $value) {
            if ($now - $key > $apc_cache_max_age) {
                unset($traffic_down_data[$key]);
            }
        }

        foreach($traffic_up_data as $key => $value) {
            if ($now - $key > $apc_cache_max_age) {
                unset($traffic_up_data[$key]);
            }
        }

        // only fetch new data if last cache entry is too old

        end($traffic_down_data); end($traffic_up_data);  // point to last element
        $last_updated = $now - min(key($traffic_down_data), key($traffic_up_data));  // also works for empty cache
        if ($last_updated >= 600) {
            list($traffic_down, $traffic_up) = countTraffic();
            $traffic_down_data[$now] = $traffic_down;
            $traffic_up_data[$now] = $traffic_up;

            //error_log("updating traffic data since last entry is " . $last_updated . " s old");
        }

        // update internal cache

        apc_store('onimon_traffic_down', $traffic_down_data, $apc_cache_ttl);
        apc_store('onimon_traffic_up', $traffic_up_data, $apc_cache_ttl);

        return [$traffic_down_data,$traffic_up_data];
    }

    function getOverTimeData() {
        global $log;

        // Get traffic
        list($traffic_down, $traffic_up) = getTrafficData();

        // Get connection count
        list($conn_orport, $conn_dirport) = getConnectionsData();

        // Bin entries separated into 1 hour intervals
        $traffic_down_over_time = overTime($traffic_down, 1024, true);
        $traffic_up_over_time = overTime($traffic_up, 1024, true);
        $conn_orport_over_time = overTime($conn_orport);
        $conn_dirport_over_time = overTime($conn_dirport);

        // Align arrays
        alignTimeArrays($traffic_down_over_time, $traffic_up_over_time);
        alignTimeArrays($conn_orport_over_time, $conn_dirport_over_time);

        return Array(
            'traffic_down_over_time' => $traffic_down_over_time,
            'traffic_up_over_time' => $traffic_up_over_time,
            'connections_orport_over_time' => $conn_orport_over_time,
            'connections_dirport_over_time' => $conn_dirport_over_time,
        );
    }

    function getOverTimeData10mins() {
        global $log;

        // Get traffic
        list($traffic_down, $traffic_up) = getTrafficData();

        // Get connection count
        list($conn_orport, $conn_dirport) = getConnectionsData();

        // Bin entries separated into 10 minute intervals
        $traffic_down_over_time = overTime10mins($traffic_down, 1024, true);
        $traffic_up_over_time = overTime10mins($traffic_up, 1024, true);
        $conn_orport_over_time = overTime10mins($conn_orport);
        $conn_dirport_over_time = overTime10mins($conn_dirport);

        return Array(
            'traffic_down_over_time' => $traffic_down_over_time,
            'traffic_up_over_time' => $traffic_up_over_time,
            'connections_orport_over_time' => $conn_orport_over_time,
            'connections_dirport_over_time' => $conn_dirport_over_time,
        );
    }

    function getProcessInfo() {
        global $TorPID;

        // CPU usage must be added up over threads, but /proc/$PID/stat shows sum already

        //$threads = findThreadsForPID($TorPID);
        //error_log("Tor process has " . count($threads) . " threads");

        $now = time();

        $info = parseProcStatForPID($TorPID);

        $tor_start_time = $info['start_time'];
        $tor_user_time = $info['user_time'];
        $tor_system_time = $info['system_time'];
        $tor_total_time = $tor_user_time + $tor_system_time;

        $sysinfo = parseProcStat();

        $total_time = $sysinfo['total_time'];
        $work_time = $sysinfo['work_time'];
        $idle_time = $sysinfo['idle_time'];
        $wait_time = $sysinfo['wait_time'];

        // If Tor restarts, scales in the chart will be off because
        // usage time for the new Tor process is much smaller than
        // before. We compensate this by scaling CPU usage with the
        // running time of the Tor process.
        // --> This approach seems to be too unreliable.

        //$tor_uptime_ratio = ($total_time - $tor_start_time) / ($total_time);

        return Array(
            'tor_uptime_ratio' =>  $tor_uptime_ratio,
            'tor_total_time' => $tor_total_time,
            'tor_user_mode_time' => $tor_user_time,
            'tor_kernel_mode_time' => $tor_system_time,
            'other_processes_time' => $work_time-$tor_total_time,
            'total_work_time' => $work_time,
            'total_idle_time' => $idle_time,
            'total_wait_time' => $wait_time,
            'total_time' => $total_time,
        );
    }

    function getMemoryInfo() {
        global $TorPID;

        // memory is shared between threads

        $info = parseProcStatmForPID($TorPID);

        $memory_total = $info['total_memory'];
        $memory_physical = $info['resident_memory'];
        $memory_shared = $info['shared_memory'];

        $system_total = floatval(exec("cat /proc/meminfo | grep ^MemTotal | awk '{ print $2 }'"));
        $system_free = floatval(exec("cat /proc/meminfo | grep ^MemFree | awk '{ print $2 }'"));
        $system_buffers = floatval(exec("cat /proc/meminfo | grep ^Buffers | awk '{ print $2 }'"));
        $system_cached = floatval(exec("cat /proc/meminfo | grep ^Cached | awk '{ print $2 }'"));
        $system_processes = $system_total - $system_free - $system_cached;
        $system_used = $system_total - $system_free - $system_buffers - $system_cached;

        $tor_usage_ratio = $memory_physical / ($system_processes*1024);

        return Array(
            'tor_physical_memory' => $memory_physical,
            'tor_virtual_memory' => $memory_total,
            'tor_effective_memory' => $tor_usage_ratio*($system_used*1024),
            'other_processes_memory' => (1-$tor_usage_ratio)*($system_used*1024),
            'free_memory' => $system_free*1024,
            'cached_memory' => ($system_cached+$system_buffers)*1024,
        );
    }

    function getAllNodes($orderBy) {
        global $log;
        $allNodes = Array("data" => Array());

        if(isset($_GET["from"]))
        {
            $from = new DateTime($_GET["from"]);
        }
        if(isset($_GET["until"]))
        {
            $until = new DateTime($_GET["until"]);
        }
        if(isset($_GET["filter"]))
        {
            $filter = $_GET["filter"];
        }

        ob_start();
        passthru("onimon nodes");
        $nodes = explode("\n", ob_get_clean());

        // pre-process query result
        $nodeList = Array();

        foreach ($nodes as $entry) {
            $fields = explode(' ', $entry);
            if ($fields[0] == 'r') {
                $ident = $fields[2]; // re-used for following lines
                $nodeList[$ident] = Array(
                    'name'      => $fields[1],
                    'ident'     => $fields[2],
                    'digest'    => $fields[3],
                    'published' => new DateTime($fields[4] . ' ' . $fields[5]),
                    'address'   => $fields[6],
                    'orport'    => intval($fields[7]),
                    'dirport'   => intval($fields[8]),
                    'flags'     => Array(),
                    'speed'     => "",
                );
            }
            else if ($fields[0] == 's') {
                $nodeList[$ident]['flags'] = array_slice($fields, 1);
            }
            else if ($fields[0] == 'w') {
                foreach ($fields as $item) {
                    if (substr($item, 0, 10) == "Bandwidth=") {
                        $nodeList[$ident]['speed'] = intval(substr($item, 10));
                        break;
                    }
                }
            }
        }

        //error_log("nodelist has " . count($nodeList) . " items");

        foreach ($nodeList as $node) {
            $name    = $node['name'];
            $ident   = $node['ident'];
            //$digest  = $node['digest'];
            $time    = $node['published'];
            $address = $node['address'];
            $orport  = $node['orport'];
            $dirport = $node['dirport'];
            $flags   = $node['flags'];
            $speed   = $node['speed'];

            while (strlen($ident)%4 > 0) $ident .= "=";  // Tor removed padding
            $fingerprint = strtoupper(bin2hex(base64_decode($ident)));

            // Check if we want to restrict the time where we want to show nodes
            if(isset($from))
            {
                if($time <= $from)
                {
                    continue;
                }
            }
            if(isset($until))
            {
                if($time >= $until)
                {
                    continue;
                }
            }

            // Check if we want to filter by flag
            if(isset($filter))
            {
                if(! in_array($filter, $flags))
                {
                    continue;
                }
            }

            $status = "";

            if($orderBy == "orderByName"){
              //$allQueries['data'][hasHostName($client)][$domain][$time->format('Y-m-d\TH:i:s')] = $status;
            }elseif ($orderBy == "orderByIdent"){
              //$allQueries['data'][hasHostName($client)][$time->format('Y-m-d\TH:i:s')][$domain] = $status;
            }elseif ($orderBy == "orderByTimePublished"){
              //$allQueries['data'][$time->format('Y-m-d\TH:i:s')][hasHostName($client)][$domain] = $status;
            }elseif ($orderBy == "orderByAddress"){
              //$allQueries['data'][$time->format('Y-m-d\TH:i:s')][$domain][hasHostName($client)] = $status;
            }elseif ($orderBy == "orderByORPort"){
              //$allQueries['data'][$domain][hasHostName($client)][$time->format('Y-m-d\TH:i:s')] = $status;
            }elseif ($orderBy == "orderByDirPort"){
              //$allQueries['data'][$domain][$time->format('Y-m-d\TH:i:s')][hasHostName($client)] = $status;
            }elseif ($orderBy == "orderBySpeed"){
              //$allQueries['data'][$domain][$time->format('Y-m-d\TH:i:s')][hasHostName($client)] = $status;
            }else{
              array_push($allNodes['data'], Array(
                $name,
                $fingerprint,
                $time->format('Y-m-d H:i:s'),
                $address,
                $orport,
                $dirport,
                implode(' ', $flags),
                $speed,
              ));
            }
        }
        return $allNodes;
    }

    function tailTorLog($param) {
        // Not using SplFileObject here, since direct
        // usage of f-streams will be much faster for
        // files as large as the pihole.log
        global $logListName;
        $file = fopen($logListName,"r");
        $offset = intval($param);
        if($offset > 0)
        {
            // Seeks on the file pointer where we want to continue reading is known
            fseek($file, $offset);
            $lines = [];
            while (!feof($file)) {
                array_push($lines,fgets($file));
            }
            return ["offset" => ftell($file), "lines" => $lines];
        }
        else
        {
            // Locate the current position of the file read/write pointer
            fseek($file, -1, SEEK_END);
            // Add one to skip the very last "\n" in the log file
            return ["offset" => ftell($file)+1];
        }
        fclose($file);
    }

    /******** Private Members ********/
    function getORConnections() {
        $ORPort = exec("onimon config ORPort");
        return intval(exec("netstat -ant | grep ESTABLISHED | awk '{print $4}' | grep ':$ORPort' | wc -l"));
    }

    function getDirConnections() {
        $DirPort = exec("onimon config DirPort");
        return intval(exec("netstat -ant | grep ESTABLISHED | awk '{print $4}' | grep ':$DirPort' | wc -l"));
    }

    function countTraffic() {
        $data = explode(' ', exec("onimon traffic"));
        return [floatval($data[0]),floatval($data[1])];
    }

    function countTrafficDown() {
        return floatval(exec("onimon traffic down"));
    }

    function countTrafficUp() {
        return floatval(exec("onimon traffic up"));
    }

    function findThreadsForPID($pid) {

        $threads = array();

        $path = "/proc/" . $pid . "/task";
        $dir = dir($path);

        while (false !== ($entry = $dir->read())) {
            if ($entry != '.' && $entry != '..') {
               if (is_dir($path . '/' . $entry)) {
                    $threads[] = $entry;
               }
            }
        }

        return $threads;
    }

    function parseProcStat() {
        // https://www.kernel.org/doc/Documentation/filesystems/proc.txt
        $hertz = floatval(exec("getconf CLK_TCK"));

        $data = explode(' ',exec("cat /proc/stat | head -n 1"));

        while ($data[0] == 'cpu' || empty($data[0]))
            array_shift($data);

        $user = intval($data[0]);
        $nice = intval($data[1]);
        $system = intval($data[2]);
        $idle = intval($data[3]);
        $iowait = intval($data[4]);
        $irq = intval($data[5]);
        $sirq = intval($data[6]);
        $steal = intval($data[7]);

        $work = $user + $system + $nice;
        $wait = $iowait + $irq + $sirq + $steal;
        $total_time = $work + $wait + $idle;

        return Array(
            'user_time' => $user / $hertz,
            'system_time' => $system / $hertz,
            'nice_time' => $nice / $hertz,
            'work_time' => $work / $hertz,
            'wait_time' => $wait / $hertz,
            'idle_time' => $idle / $hertz,
            'total_time' => $total_time / $hertz,
        );
    }

    function parseProcStatForPID($pid) {
        // https://www.kernel.org/doc/Documentation/filesystems/proc.txt
        $hertz = floatval(exec("getconf CLK_TCK"));

        $data = explode(' ', exec("cat /proc/" . $pid . "/stat"));

        $pid = intval($data[0]);
        $tcomm = $data[1];
        $state = $data[2];
        $utime = floatval($data[13]);
        $stime = floatval($data[14]);
        $cutime = floatval($data[15]);
        $cstime = floatval($data[16]);
        $priority = intval($data[17]);
        $nice = intval($data[18]);
        $num_threads = intval($data[19]);
        $start_time = floatval($data[21]);
        //$blkio_ticks = floatval($data[40]);

        $total_time = $utime + $stime;
        $total_ctime = $cutime + $cstime;

        return Array(
            'pid'           => $pid,
            'name'          => $tcomm,
            'state'         => $state,
            'priority'      => $priority,
            'nice'          => $nice,
            'num_threads'   => $num_threads,
            'user_time'     => $utime / $hertz,
            'system_time'   => $stime / $hertz,
            'total_time'    => $total_time / $hertz,
            'total_ctime'   => $total_ctime / $hertz,
            'start_time'    => $start_time / $hertz,
        );
    }

    function parseProcStatmForPID($pid) {
        // https://www.kernel.org/doc/Documentation/filesystems/proc.txt
        $pageSize = floatval(exec("getconf PAGE_SIZE"));

        $data = explode(' ', exec("cat /proc/" . $pid . "/statm"));

        $size = floatval($data[0]);
        $resident = floatval($data[1]);
        $shared = floatval($data[2]);

        $total_memory = $size * $pageSize;
        $resident_memory = $resident * $pageSize;
        $shared_memory = $shared * $pageSize;

        return Array(
            'total_memory'      => $total_memory,
            'resident_memory'   => $resident_memory,
            'shared_memory'     => $shared_memory,
        );
    }

    function excludeFromList(&$array,$key)
    {
        global $setupVars;
        $domains = explode(",",$setupVars[$key]);
        foreach ($domains as $domain) {
            if(isset($array[$domain]))
            {
                unset($array[$domain]);
            }
        }
        return $array;
    }

    function overTime($data, $scale=1, $derive=false) {
        $byTimeData = [];
        $datetime = new DateTime();
        $timeOffset = 0;
        $lastValue = 0;
        $lastHour = 0;
        $lastTimestamp = time();
        foreach ($data as $timestamp => $value) {
            if ($timestamp > $lastTimestamp) {
                $datetime->setTimestamp($timestamp);
                $hour = $datetime->format('G');

                $hour += $timeOffset;
                if ($hour < $lastHour) {
                    $timeOffset += 24;
                    $hour += 24;
                }
                if (array_key_exists($hour, $byTimeData))
                    continue;

                if ($derive == true) {
                    $derivedValue = floatval($value-$lastValue) / ($timestamp-$lastTimestamp);
                    if ($derivedValue >= 0)
                        $byTimeData[$hour] = $derivedValue / $scale;
                }
                else {
                    $byTimeData[$hour] = $value / $scale;
                }
            }
            $lastTimestamp = $timestamp;
            $lastValue = $value;
        }
        return $byTimeData;
    }

    function overTime10mins($data, $scale=1, $derive=false) {
        $byTimeData = [];
        $datetime = new DateTime();
        $timeOffset = 0;
        $lastValue = 0;
        $lastTime = 0;
        $lastTimestamp = time();
        foreach ($data as $timestamp => $value) {
            if ($timestamp > $lastTimestamp) {
                $datetime->setTimestamp($timestamp);
                $hour = $datetime->format('G');
                $minute = $datetime->format('i');

                // 00:00 - 00:09 -> 0
                // 00:10 - 00:19 -> 1
                // ...
                // 12:00 - 12:10 -> 72
                // ...
                // 15:30 - 15:39 -> 93
                // etc.
                $time = ($minute-$minute%10)/10 + 6*$hour + $timeOffset;
                if ($time < $lastTime) {
                    $timeOffset += 144;
                    $time += 144;
                }

                if (array_key_exists($time, $byTimeData))
                    continue;

                if ($derive == true) {
                    $derivedValue = floatval($value-$lastValue) / ($timestamp-$lastTimestamp);
                    if ($derivedValue >= 0)
                        $byTimeData[$time] = $derivedValue / $scale;
                }
                else {
                    $byTimeData[$time] = $value / $scale;
                }
                $lastTime = $time;
            }
            $lastValue = $value;
            $lastTimestamp = $timestamp;
        }
        return $byTimeData;
    }

    function alignTimeArrays(&$times1, &$times2) {
        if(count($times1) == 0 || count($times2) < 2) {
            return;
        }

        $max = max(array_merge(array_keys($times1), array_keys($times2)));
        $min = min(array_merge(array_keys($times1), array_keys($times2)));

        for ($i = $min; $i <= $max; $i++) {
            if (!isset($times2[$i])) {
                $times2[$i] = 0;
            }
            if (!isset($times1[$i])) {
                $times1[$i] = 0;
            }
        }

        ksort($times1);
        ksort($times2);
    }
?>
