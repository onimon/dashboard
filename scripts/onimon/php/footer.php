        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
            <?php
            $torVersion = exec("onimon version");

            // Check if on a dev branch
            $webBranch = exec("git rev-parse --abbrev-ref HEAD");

            if($webBranch !== "master") {
                $webVersion = "vDev";
                $webCommit = exec("git describe --long --dirty --tags");
            }
            else {
                $webVersion = exec("git describe --tags --abbrev=0");
            }
            ?>
        <div class="pull-right hidden-xs">
            <b>Tor Version </b><span id="torVersion"><?php echo $torVersion; ?></span>
            &nbsp;|&nbsp;
            <b>Web Interface Version </b><span id="webVersion"><?php echo $webVersion; ?></span><?php if(isset($webCommit)) { echo " (".$webBranch.", ".$webCommit.")"; } ?>
        </div>
        <div><b><a href="https://www.torproject.org/">Tor Project</a></b> - <i>Anonymity Online</i></div>
    </footer>
</div>
<!-- ./wrapper -->
<script src="scripts/vendor/jquery.min.js"></script>
<script src="scripts/vendor/jquery-ui.min.js"></script>
<script src="style/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="scripts/vendor/app.min.js"></script>

<script src="scripts/vendor/jquery.dataTables.min.js"></script>
<script src="scripts/vendor/dataTables.bootstrap.min.js"></script>
<script src="scripts/vendor/Chart.bundle.min.js"></script>

<script src="scripts/onimon/js/footer.js"></script>

</body>
</html>
